import 'dart:developer';

import 'package:maidatpackages/bignumber_util.dart';
import './string_util.dart';

void main() {
  var s = ' Xin chào Mai Lê Tiến Đạt  ';
  log(StringUtil.removeVietnameseSymbol(s));
  log(StringUtil.countCharacters(s).toString());

  var a = 123;
  var b = 1229;
  log(BigNumber.sum(a, b));
}

