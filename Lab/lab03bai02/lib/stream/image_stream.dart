import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../model/image.dart';

String jsonRaw = 'https://jsonplaceholder.typicode.com/photos';

class ImgStream{
  final List<ImageModel> images = [];

  Stream<ImageModel> getImages() async* {
    
    fetchImages();
    yield* Stream.periodic(Duration(seconds: 5), (int t){
      int index = t%7;
      return images[index];
    });

  }

  fetchImages() async{
    final response = await http.get(Uri.parse(jsonRaw));

    if(response.statusCode == 200){
      for ( final jsonObject in jsonDecode(response.body)){
        images.add(ImageModel.fromJson(jsonObject));
      }
      
    }
    else{
      throw Exception('Failed to load image');
    }
  }
}