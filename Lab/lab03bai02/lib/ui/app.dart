import 'package:flutter/material.dart';
import 'package:lab03bai02/stream/image_stream.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Image Stream",
        home: Scaffold(
          body: TextScreen(),
        )
    );
  }

}

class TextScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return TextState();

  }

}

class TextState extends State<StatefulWidget>{
  ImgStream imgStream = ImgStream();
  String? imgUrl = 'https://thumbs.dreamstime.com/b/funny-orange-cat-says-lets-go-cute-emoticon-character-vector-illustration-funny-orange-cat-says-lets-go-vector-illustration-123844464.jpg';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Messages',

      home: Scaffold(
        appBar: AppBar(title: Text('Streaming'),),
        body: Image.network(imgUrl!),
        floatingActionButton: FloatingActionButton(
          child: Text('Start'),
          onPressed: (){
            streamMessages();
          }
          ),
        ),
    );
  }

  streamMessages() async{
    imgStream.getImages().listen((event) {
      setState(() {
        imgUrl =  event.url;
      });
    });
  }
  
}