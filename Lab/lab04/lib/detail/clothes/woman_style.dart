import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WomenClothesDetail extends StatefulWidget{
  static const route = '/clothes/women';
  @override
  State<StatefulWidget> createState() {
    return WomenClothesDetailState();
  }

}

class WomenClothesDetailState extends State<WomenClothesDetail>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget> [
          Container(
            height: 70,
            color: Colors.blue,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
                Container(
                  width: 300,
                  child: CupertinoSearchTextField(
                    backgroundColor: Colors.white,
                    placeholder: 'Thời trang nữ',
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 795,
            child: CustomScrollView(
              primary: false,
              slivers: <Widget>[
                SliverPadding(
                  padding: EdgeInsets.all(10),
                  sliver: SliverGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing:  10,
                    mainAxisSpacing: 10,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://sakurafashion.vn/upload/sanpham/large/36428-ao-nu-hoa-tiet-con-bo-1.jpg')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://mcdn.nhanh.vn/store1/45885/ps/20200827/screenshot_1598493388.png')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://sakurafashion.vn/upload/sanpham/large/3827-ao-kieu-thuy-thu-1.jpg')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://sakurafashion.vn/upload/sanpham/large/19794-ao-nu-kieu-thuy-thu-dinh-quang-co-1.jpg')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://img.ltwebstatic.com/images2_pi/2019/06/24/15613598991285625988.webp')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://cf.shopee.vn/file/ebaa482d60b2e717545f286f818b8d65')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://cf.shopee.vn/file/591a0853b958dd13fd55092615b17cef')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8SEhUQEBAVFRUVFRUQFQ8PDw8PDw8VFRUXFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0NFxAPGislFSUtKysyNy0tLS8tLTUtLS0rKzAwLS0rKys3LS0tKystKzAtLSsvLTctLTQrLC0tKy0rK//AABEIAQoAvgMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAAECBAUGB//EAEMQAAIBAgMEBwYDBQQLAAAAAAABAgMRBCExBRJBUQYiYXGBkbETMlKhwdFCcpIjgsLh8BRiY/EWMzRDc5OisrPS4v/EABgBAQEBAQEAAAAAAAAAAAAAAAADAgEE/8QAHREBAAIDAAMBAAAAAAAAAAAAAAECAxEhMWFxE//aAAwDAQACEQMRAD8A6NDsZDoBDiHAQhIQCEIQCFciM2BO4olWeIzsh/btax+dgLyFYFh6ykrryeqDAQYkSYyAYlEQ6AkiaIokgJCQw6AcQ4wGciSIImgHEIQCEIQCYwhAMyM4kxMAGHo2k2wleCZMhUkBX2X784/1/WSNOxm7Lf7Sfd9GabAhIYkyIDkrDIcB0SRFEgJIdELkosCYwkOBmIkRiSsA44khAOMxCAjcSEyNwJXIuYKpUsYm0doST6oG9KoU4VN+oot5dhq4bZ8NyO/G8t1bz3pK8rZ6PmShsyjF76h1lnfem/k3Yj+1V/wsr0IQi+qrXLNwEsPFcPJy+4Si9ctPH+uArmradFsFqxtJsSItjxLIJoQkSQCSHEPYBh0OMA6J3IDoDOiTRBE0BIYcYBDMTZGUgE2CnIUplLFYpJARxteyKuw8C61X2kl1IO+eknwX1I4TCVMTLLKCedTh3R5v0OtwuHjTioQVkl/m2/qRy5Ncjyvix77PgZMdO+VsuJVq4hL3c3zengUMRVqvSpJfle76E4w2lW2asS0d640UYE8ViIO+9vrlNZ/qWfqXsDteFTqvqz+GWr/L8Xr2E7Yr06pXLS/FyrGz/rPtFFhLpqz8JLh90CmmtfPg+5nqx5IvHt5MuKaT6FRJA4MJEokkhxCAQw4mAw4hAZyJJkEPcAlxnIG5gKtZIA06hWq4hcyrUxLk1GCbk3ZRirt+Br7O6K1J2niJWWvs4PPxl9vMDNo1J1JblOLlLs0Xa3ol3lql0cnvv+0vR5U4NtS5NvWz5efI7LB7PpUo7lOCiuSWva3xYTF4Vzjk0pr3ZtX8GZtEzHG6TET1gSUKcUtLLq042Tt3cEU6uIcuxclp/MbGUZwk1UTUtc879qfFdoFHKY4r9avkm3wUZoSY9zaQFSmZmNwKfA15MrVZIDKobVq0Xapeced/2kfF+93PzRv4LH06sbxalHitHF9q1iznsc0yPR3YmKxFW+GvFJ2nXd1Tprin8T/u+mpG+GJndeSvTNMct2HXbOwVSpUcKbTio7znJ2cOUZLm87Na2elixWoSg92as+3R9z4m7gsHChTVKlz3pTfvVJPWT+3BZBpNNbs0pLimrla713ylbW+eHNWGNTF7Lst6k3JcYPOUe7n695mNHWTCYhAIdDDoDLQmxiMmAKvUsZOJxPAt4yQLYeE9rXjFq6XXl3Rzt4uy8QOw6LbFjSgqslecopttZxuk91ckvn5W34Zj4P3V2X8n/P1BUJdawFhIlYlYawAcThYVI7s4qS7cnHtT1Rg4vo7NZ0ZqS+CfVl4PR/I6WxCQHFVsBiIe9Qqd8YOa843K8t9f7ufjTn9jvoSJ7z5gedSjWfu0ar7qNV/QUdiY6plHDyXbUcaaX6nf5Hoyb5kW2ByezehME1LF1N96+xpXUP3p5N+FvE6inGMYqnTioQWShBKMV4IU3YfhcBrEZRJwzHnECu5uLuVNsYRZVYrqySvbhJ6+ZYrs0aWHUqG4+Kfg+Hll5AceOPUjZ2f+REBCQhMDKYObJsHMChjDZ6G4eyqVebVNeC3pesfIxcSdfsShuUIR4uO+++XW+qXgBv4X3Svhn1n+awei+qVMNL9pJdqYGxYhJBYkZoCLWRXqssyWRVrMCdJhGApMsICcERZOKITAq1pZhorqlWs8y3QeQAqEsyzWWVyo8mXJvqgZs85Jcs2bOC90xaOsn228v8zbwqyQHMbcobtWXJ9deOvzuZp0nSSllGfJuL8c16PzOdkgIokiJJAZAOoEYCswKtKj7SpGHxSUX3Xz+VzurHLdG6O9Wcvgi34y6q+W8dZFZB1ZovqlZZVV2r0C0HlYHNftIfvegcbcB5IjSCtABmU65dmUsQA1FlmJVoFqABkCqBgNUDPrvMuYR5FGu8y3gmAsQsyzDOILEoJh/dYGbRedu/1N3D6GBQfXfZ9zfo6AV9r0t6lNdm8v3c/ocm0dtURx1enuycX+FuPkBXkhibIgYzK2IeRYkyni5Ab3RejanKfxSsu6Kt6uRv0lkUcBQ9nThDioq/e85fNs0qK6oAqbH/HHub+aIiou9Tuj9UBtUQzBUAzADUM/EGhUM+uA1EtwKlItwAMCraBEwVYDNralnAsq1dQ+BeYFzEIlhB8QshsIBlYJdeb/AL1vm/sb9Ex8NC0p/wDEl8ma9FgEqHNbepbtTe4SSfisn6LzOmqGPt+lempfC8+6WXqkBzzGJWIgYU2Q2fR9pWhHhe77o5v0t4kK9Q2ejmAlG9adrSj1c81Fu7b5XsrAbj1LUfdKsdX4FqfugV2x8HnOXckDvmE2d70u9fUDcw6DSBYfQLIAFQz62poVDPqgRplyBTpluLyAJFg6xKBCqBm1dQuEeYCs+sEoPMDWrLIhhdQmsSGEeYFdq05/mk/Nl7DSKuIVpT716ILg5AXp6FSvS34yhzTXjw+di3wAPJgcc+RFo0Nr4ZxqOSXVk7p8LvNrzZRYHMbOwrrVVDhrJ8orX7eJ2tWSXVWls+SXL6GZ0awXs6XtGutPPuj+FfXxXIu4mVl2sAmCbzfxS9EXMXLdTtzSt32f3K+Djbd/rULtN8ObuBUVRb1uO7vdnAs7O1l3r6lKL68vyJfMvbP4+AG3h9A0wOG0DVAK9TQoVS/UKNcAdMtRKtMsoCcWQq6DxGqsDKrvrBYagcRqFpvQDYovIhSykPh9BuIAsfO0pX5r0Q+FqcEuNu4p7aylJ9kH6fYs4TW/PP5IDTu9CtinutS7bPxLPBMHjIXjYCtXoKcXTfFb0ZcnwOaqwadmrNZNcjpqU9E+BQ23hc1Uitcnbnwfl6AV0srFLEu8ki5J2RToq87gaFFZojtOWaJUdQO1Hmu4ClRl+0fc/v8AQ09n6eJk4X3/AD9DYwC9QNrDaBqgLD6BZgVplKuXqmhSr6ABpFgBhw60AeA1YVJkaoGZX1J0XkQrrMeg+AGzhdBTIYCV0FqoDK288/3I+rD7MleMX2ehX2/w/Iv+6QTZrtGIG5SfAnUjkCpMOBmyjZh6E8hq8cwdN2Awq7yIYOOrHrhMOrRANR1K+1PoWaQHHxugM3Be8beD+pjYVWkbWF18QNjD6BWBoBmwA1FlkU6qyaDYqk5dXefhkjGxGC3Xrx4oC9Qhb5/yDUabs0Z1Ci+wW39ne2w86SajvpQc0lLdW/Ft245JgaEI2ybS45ySyB1Jx+KP6kYmxOhmCwk3VpKTqSg4TnOd7xbTskkks4rQ2qsI6buXbmBQqpN5NPuaYOLs7kcRRi3fdXkiEpNfzzA1sFUs7GjUV8zAw+IzXBm7h6l1mBk7cj7v5f4mDwEuquwtbahmvyr1ZSwTyaA6DCyui0jPwMjQQAMRErNF2ssirUiBz091tJSWeivZvuCnE4fFVKeUXlxhJb0H4cPA3cDt+m7KonHtd5R/Vr5rxA6GghsRDIjhsTSkrxndc11l5xuTqV6dvfXzAzIwtLxCbaqyhh6soNqSUUpRbi03UirprsZOSTeTuB6Qf7NLtnCPz3v4QMTD9L8bG1/ZztxnTs34xaNSl05qfiw0X2xqyj8nFnLqkEjADqv9M46/2eX/ADY/+oLFdM6FrzoVO7epteZzNWaSNHo5sf2jjiauizp0+bTynK/y8HyA3MPjJVUpLD+zctI1J70kuDaSVn2XL8Xa0N67b4ybs1GWq5XVvBlbHY2lhqcq9V2SWSjnJt5JRXNvJfZMq9E6s69CWJmrOpWk1BZqEIRjCEE+NrSd+LbfEAmB2zKc5U6kPZunOdNNT34tKV4ZtK14tPxsbuGq36srPt5nJ7NtUqYmXLFVIfpjTj9AuM2jUw1aH4qcoe5yabu4vuay0A6HaWBm86TgnyqRlKL8U7ryZy+0a+Mp/wCspRSvlJRk4PukpW8DqsFtihWStKz+GWTMjpfW9yiuF6ku95R+W95gYUdq1eMYfpkv4iNTpbiKOcYxfZeaXldgZIx9qq/mB6Lh8dKvRp1pJJzhGTUb2TazSIUsmV+i0t7C0uyO7+ltfQuzhYC/gJ5mvEwcJKxuU5XSAeQFK2un1Ku0trQpdVdafwp5R/M+HcczjMXOq7zd+S/DHuQHM7gvZhBwBQi4u8W4vnFuL80XqW0q6yc9786T+evzK6QrAaEdrS4wj4by+5HaG05VYKm4JLeU7qTbyTVvmURAQsIkIANaF0Wej2NlQk4yu6T4aum+cVy5rx53GyE2BU6V7QlXmm8orKMOS5vnI77oPRtgqK578vOrM812hqj1Do5UVPAUpvSNJz+cmBz/AEVe9Vxcf8d1f1q38Bf6UYa8Kc/hk4/qV/4TL6IT3cVVg/x0oz/ROz/8h0m24J4ed+Di1fnvL6NgcrFWJTberb7W7iHABUiVKuHT1L7Q26BZ2PjXRjuKN1dv3rWvw055mitsf4f/AF//ACZEUEQGmtrS4QS75N/YeptevJbu9urlBWfnqZlySYBbjXGuNcDKEMOA6HGQ4DMYdiAYQmICLYKowkgFZgZuLfWPStlz3tlxf+FKP6ZOK9DzSrC8j0LAT3dlwXO8fOtJ+iYGPhJezqxrR1ipRs9Gpap+SfgGxeLnVe9N35LSMe5FVMkmA47GHAZjDsQDolcimOmBJDpkR7gTTFcgmPcDNEmFUVyGaXICCHYVRXITiuQA7iCqK5D7q5AV2INurkLdXICvIBUiXZRXIg4rkBQhh7s6SeLX9lpUU81OcpLirPq3799+RmwiuXyCqK5AQTCJkt1ch1FcgIXHTJ7q5DqK5AQEEsuQ9lyAChwllyHcVyAGK4Wy5Dbq5AQTHuTsuQrID//Z')
                                )
                            )
                          ),
                          Text('200.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo nữ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}