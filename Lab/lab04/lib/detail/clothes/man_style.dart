import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenClothesDetail extends StatefulWidget{
  static const route = '/clothes/men';
  @override
  State<StatefulWidget> createState() {
    return MenClothesDetailState();
  }

}

class MenClothesDetailState extends State<MenClothesDetail>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget> [
          Container(
            height: 70,
            color: Colors.blue,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
                Container(
                  width: 300,
                  child: CupertinoSearchTextField(
                    backgroundColor: Colors.white,
                    placeholder: 'Thời trang nam',
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 795,
            child: CustomScrollView(
              primary: false,
              slivers: <Widget>[
                SliverPadding(
                  padding: EdgeInsets.all(10),
                  sliver: SliverGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing:  10,
                    mainAxisSpacing: 10,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://sakurafashion.vn/upload/images_665/F115641C-E330-486B-9144-51A46FF87B66.jpeg')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://smartfashion.ai/wp-content/uploads/2020/09/shop-quan-ao-nam-dep.jpg')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRitkapIPoFIxxVJEkJFFU6L_2reBEgTc9PsdM-MQ_jmUYNjqT189oQsi10dpG2c0DJ0Yg&usqp=CAU')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://cf.shopee.vn/file/3cdc0cfb29666abb974ae18050e979f5')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://4men.com.vn/thumbs/2022/01/ao-so-mi-in-hoa-tiet-lon-asm081-mau-trang-20351-p.JPG')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROK_6fzDraBv7XOmNNynDscdOu9JcoQTlY15BLfTD4WDUdItmYV2qgRFEEyCDGUk3cZoo&usqp=CAU')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJThP2uwPIU51ZsI9IjDEgszpAUivqppSwMD-Lh-anNNF4A6ea0t4AXJnJlwZH0vBHGGA&usqp=CAU')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://4men.com.vn/thumbs/2022/02/ao-so-mi-in-hoa-tiet-asm083-mau-den-20374-p.JPG')
                                )
                            )
                          ),
                          Text('150.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Áo sơ mi nam', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}