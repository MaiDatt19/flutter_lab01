import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FootballSportsDetail extends StatefulWidget{
  static const route = '/sports/football';
  @override
  State<StatefulWidget> createState() {
    return FootballSportsDetailState();
  }

}

class FootballSportsDetailState extends State<FootballSportsDetail>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget> [
          Container(
            height: 70,
            color: Colors.blue,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
                Container(
                  width: 300,
                  child: CupertinoSearchTextField(
                    backgroundColor: Colors.white,
                    placeholder: 'Bóng đá',
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 795,
            child: CustomScrollView(
              primary: false,
              slivers: <Widget>[
                SliverPadding(
                  padding: EdgeInsets.all(10),
                  sliver: SliverGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing:  10,
                    mainAxisSpacing: 10,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://www.thethaothientruong.vn/uploads/images/bong-da-dong-luc-fifa-uhv-207.07.jpg')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://sportonline.vn/wp-content/uploads/2018/04/qua-bong-da-FIFA-Quality-Pro-UHV-2-07-GALAXY.jpg')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://www.thethaothientruong.vn/uploads/contents/bong%20dong%20luc%20UVH%20%202_04.jpg')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://aolikesvietnam.com/wp-content/uploads/2018/09/bong-da-so-5-da-pu-cao-cap-1.jpg')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://cdn.yousport.vn/Media/Products/050320104659560/qua-bong-da-uhv2-16-so-5_large.jpg')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('http://belo.vn/wp-content/uploads/2017/07/ucv3-17.jpg')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://belo.vn/wp-content/uploads/2019/01/z1251547605780_5d45c53b2bb02e74f5d9642df41dfe37.png')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        color: Color.fromARGB(255, 240, 248, 250),
                        child: Column(children: [
                          Container(
                              width: 160.0,
                              height: 130.0,
                              decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuXN_6m-sFdWoUs7V8n7LC8zXiDWZBtwY6Siql8jKW6B82Wl9eNB6e9hklfGEHtSEPzmI&usqp=CAU')
                                )
                            )
                          ),
                          Text('250.000 vnd', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.red),),
                          Text('Quả bóng đá', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
                        ],)
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}