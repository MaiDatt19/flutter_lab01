import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:lab04/detail/clothes/man_style.dart';
import 'package:lab04/detail/clothes/woman_style.dart';
import 'package:lab04/detail/sports/basketball.dart';
import 'package:lab04/detail/sports/football.dart';
import 'package:lab04/main.dart';
import 'package:lab04/page_detail/clothes.dart';
import 'category.dart';
import 'home.dart';
import './hot.dart';
import './sale.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Routing App',
      routes: {
        //khai bao cac duong dan
        CategoryScreen.route: (context) => CategoryScreen(),
        ClothesDetail.route: (context) => ClothesDetail(),
        MenClothesDetail.route: (context) => MenClothesDetail(),
        WomenClothesDetail.route: (context) => WomenClothesDetail(),
        FootballSportsDetail.route: (context) => FootballSportsDetail(),
        BasketballSportsDetail.route: (context) => BasketballSportsDetail()
        //'/category_item':(context) => CategoryItemScreen()
      },
      initialRoute: '/',
      home: AppHomePage(),
    );
  }
  
}


class AppHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<AppHomePage>{
  int currentIndex = 0;
  final screens = [
    HomeScreen(),
    CategoryScreen(),
    HotScreen(),
    SaleScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: IndexedStack( //-> switch giua cac trang
          index: currentIndex,
          children: screens,
        ),
        bottomNavigationBar: BottomNavigationBar(//=> bottom navigation bar
          type: BottomNavigationBarType.fixed,
          iconSize: 35,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.blue,
          onTap: (index){
            setState(() {
              currentIndex = index;
            });
          },
          currentIndex: currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label:  "Home",
              backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label:  "Category",
              backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.auto_awesome),
              label:  "Hot!!!",
              backgroundColor: Colors.blue
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.local_fire_department_sharp),
              label:  "On Sale",
              backgroundColor: Colors.blue
            ),
          ]
        ),
      )
      
    );
  }

}
