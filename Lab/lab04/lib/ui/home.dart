import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './category.dart';
import './hot.dart';
import './sale.dart';

class HomeScreen extends StatefulWidget{
  static const route = '/';

  @override
  State<StatefulWidget> createState() => HomeScreenState();
  
}

class HomeScreenState extends State<HomeScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:buildHomeScreen(),         
    );
  }

  Widget buildHomeScreen(){
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors:[Colors.lightGreen, Colors.lightBlue] 
          ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("FREESHIP +", 
              style: TextStyle(
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.white
                ),
              ),
              SizedBox(width: 70,),
              Image.network('https://theme.hstatic.net/200000103731/1000589374/14/tiki.png?v=137', height: 40, width: 40,),
              // Text("FREESHIP", 
              // style: TextStyle(
              //   fontStyle: FontStyle.italic,
              //   fontSize: 15,
              //   color: Colors.white
              //   ),
              // ),
              SizedBox(width: 70,),
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.notification_add_outlined)
              ),
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.shopping_cart)
              )
            ],
          ),
          SizedBox(height:10),
          CupertinoSearchTextField(
            backgroundColor: Colors.white,
            placeholder: 'Bạn tìm gì hôm nay?',
          ),
          Container(
            height: 100.0,
            child: ListView(
              padding: EdgeInsets.all(5),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Thịt, Rau Củ', style: TextStyle(
                      fontSize: 15,
                      color: Colors.white
                      ),
                    ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Bách Hóa', style: TextStyle(
                      fontSize: 15,
                      color: Colors.white
                      ),
                    ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Nhà Cửa', style: TextStyle(
                      fontSize: 15,
                      color: Colors.white
                      ),
                    ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Điện tử', style: TextStyle(
                      fontSize: 15,
                      color: Colors.white
                      ),
                    ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Thiết Bị', style: TextStyle(
                      fontSize: 15,
                      color: Colors.white
                      ),
                    ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Quần Áo', style: TextStyle(
                      fontSize: 15,
                      color: Colors.white
                      ),
                    ),
                ),
              ]
    ),
),
          // SingleChildScrollView(
            
          //   padding: EdgeInsets.all(10),
          //   scrollDirection: Axis.horizontal,
          //   child: Row(
          //     children: [
          //       Text('Thịt, Rau Củ'),
          //       Text('Bách Hóa'),
          //       Text('Nhà Cửa'),
          //       Text('Điện tử'),
          //       Text('Thiết bị')
          //     ],
          //   ),
          // )
        ],
      ),
    );
  }

//category button
  void navigateCategory(){
    Navigator.of(context).pushReplacementNamed(CategoryScreen.route);
    setState(() {
      
    });
  }

//hot button
  void navigateHot(){
    Navigator.of(context).pushReplacementNamed(HotScreen.route);
    setState(() {
      
    });
  }

//sale button
  void navigateSale(){
    Navigator.of(context).pushReplacementNamed(SaleScreen.route);
    setState(() {
      
    });
  }
}