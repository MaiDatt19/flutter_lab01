import '../model/pizza.dart';
import '../utils/httphelper.dart';


class PizzaController {
  Future<List<Pizza>> callPizzas() async{
    HttpHelper helper = HttpHelper();
    List<Pizza> pizzas = await helper.getPizzaList();
    return pizzas;
  }
}