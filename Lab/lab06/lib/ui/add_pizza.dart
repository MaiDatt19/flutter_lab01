import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:lab06/model/pizza.dart';
import 'package:lab06/ui/app.dart';
import 'package:lab06/utils/httphelper.dart';

class AddScreen extends StatefulWidget{
  late Pizza pizza;

  AddScreen({Key? key, required this.pizza}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AddScreenState();
  }

}

class AddScreenState extends State<AddScreen>{
  var idController = TextEditingController();
  var nameController = TextEditingController();
  var desController = TextEditingController();
  var priceController = TextEditingController();

  @override
  void initState(){
    var pizza = widget.pizza;
    idController.text = (pizza.id != null) ? pizza.id.toString() : '';
    nameController.text = pizza.name;
    desController.text = pizza.description ?? '';
    priceController.text = (pizza.price != null) ? pizza.price.toString() : '';
  }

  @override
  Widget build(BuildContext context) {
    var pizza = widget.pizza;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text((pizza.id!=null) ? pizza.name : 'New Pizza'),
      ),
      body: Column(
        children: [
          inputPizzaId(),
          inputPizzaName(),
          inputPizzaDescription(),
          inputPizzaPrice(),
          buttonAdd()
        ],
      ),
    );
  }

  Widget inputPizzaId() {
    return TextFormField(
      controller: idController,
      decoration: InputDecoration(
        label: Text('Pizza\'s ID:'),
        icon: Icon(Icons.numbers)
      ),
      
    );
  }

  Widget inputPizzaName() {
    return TextFormField(
      controller: nameController,
      decoration: InputDecoration(
        label: Text('Pizza\'s Name:'),
        icon: Icon(Icons.local_pizza)
      ),
      
    );
  }


  Widget inputPizzaDescription() {
    return TextFormField(
      controller: desController,
      decoration: InputDecoration(
        label: Text('Pizza\'s Description:'),
        icon: Icon(Icons.description)
      ),
      
    );
  }

  
  Widget inputPizzaPrice() {
    return TextFormField(
      controller: priceController,
      decoration: InputDecoration(
        label: Text('Pizza\'s Price:'),
        icon: Icon(Icons.price_check)
      ),
      
    );
  }

    Widget buttonAdd() {
      return ElevatedButton(
        onPressed: (){
          addNewPizza();
        }, 
        child: Text("SAVE")
      );
  }

  void addNewPizza() async{
    HttpHelper httpHelper = HttpHelper();
    Pizza defaultPizza = Pizza.createDefault();
    var pizza = widget.pizza;

    //
    pizza.name = nameController.text;
    pizza.description = desController.text;
    pizza.price = priceController.text.isNotEmpty ? double.parse(priceController.text) : null;

    var jsonPizza = pizza.toJson();

    var res; 
    log(pizza.id.toString());
    if(pizza.id == null){
      pizza.id = idController.text.isNotEmpty ? int.parse(idController.text) : null;
      res = await httpHelper.postPizza(pizza);
      log('Added');
    }
    else{
      res = await httpHelper.putPizza(pizza);
      log('updated');
    }
    setState(() {
      Navigator.pop(context);
    });
  }

  
}