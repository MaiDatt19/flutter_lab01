import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lab06/ui/add_pizza.dart';
import 'package:lab06/ui/delete_pizza.dart';
import 'package:lab06/ui/home.dart';
import 'package:lab06/ui/login.dart';
import 'package:lab06/ui/setting.dart';

import '../model/pizza.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    Pizza defaultPizza = Pizza.createDefault();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food App',
      routes: {
        '/':(context) => AppHomePage(),
        '/login':(context) => LoginScreen(),
        '/add':(context) => AddScreen(pizza: defaultPizza,),
        '/delete':(context)=> DeleteScreen(),
      },
      initialRoute: '/login',
    );
  }
  
}

class AppHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }

}

class AppHomePageState extends State<AppHomePage>{
  int currentIndex = 0;
  final screens = [
    HomeScreen(),
    SettingScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Food Bank'),
          actions: [addPizza()]
        ),
        body: IndexedStack(
          children: screens,
          index: currentIndex,
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings'
            )
          ],
          currentIndex: currentIndex,
          onTap: (index){
            setState(() {
              currentIndex = index;
            });
          }
        ),
      )
    );
  }

  Widget addPizza() {
    Pizza defaultPizza = Pizza.createDefault();
    return IconButton(
      onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>AddScreen(pizza: defaultPizza)));
      }, 
      icon: Icon(Icons.add)
    );
  }
}