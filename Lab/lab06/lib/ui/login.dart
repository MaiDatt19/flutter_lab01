import 'package:flutter/material.dart';
import 'package:lab06/ui/app.dart';
import 'package:lab06/validation/mixin_common_validation.dart';

class LoginScreen extends StatefulWidget{
  static const route = '/login';
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<LoginScreen> with CommonValidation{
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("LOGIN", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            emailField(),
            passwordField(),
            loginButton(),
          ],
        )
      ),
    );
 
  }

  Widget emailField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: "Email address"),
      validator: validateEmail,
      onSaved: (value){
        email = value as String;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
        labelText: "Password"),
      validator: validatePassword,
      onSaved: (value){
        password = value as String;
      },
    );
  }
  
  Widget loginButton(){
    return ElevatedButton(
      onPressed: validate, 
      child: Text('SUBMIT')
      );
  }

  void validate(){
    final form = formKey.currentState;
    if(form!.validate()){
      //Navigator.of(context).pushReplacementNamed('/');
      Navigator.push(context, MaterialPageRoute(builder: (context)=>AppHomePage(), maintainState: false));
      setState(() {
        
      });
    }
    else{
      return;
    }
  }
  


}
