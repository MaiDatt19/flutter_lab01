import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:lab06/utils/httphelper.dart';

import '../model/pizza.dart';

class DeleteScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return DeleteScreenState();
  }

}

class DeleteScreenState extends State<DeleteScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DELETE MODE'),
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ),
      ),
      body: Container(
        child: FutureBuilder(
          future: readJsonFile(context),
          builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas){
            return ListView.separated(
              itemBuilder: (BuildContext context, int index){
                var item = pizzas.data![index];
                return Dismissible(
                  key: Key(item.id.toString()),
                  child: ListTile(
                    title: Text(item.name),
                    subtitle: Text('${item.description}\nPrice: ${item.price}\$'),
                  ),
                  onDismissed: (item){
                    HttpHelper httpHelper = HttpHelper();
                    httpHelper.deletePizza(item.index);
                  },
                );
              }, 
              itemCount: pizzas.data?.length ?? 0, 
              separatorBuilder: (BuildContext context, int index) => const Divider(),
            );
          }
        ),
      ),
    );
  }

  Future<List<Pizza>> readJsonFile(context) async{
    final res = await http.get(Uri.parse('https://4glvr.mocklab.io/pizzalist'));
    List<Pizza> myPizzas = [];
    if(res.statusCode == 200){
      for (final jsonObj in jsonDecode(res.body)){
        myPizzas.add(Pizza.fromJson(jsonObj));
      }
    }
    else{
      throw Exception('Cant load pizzas');
    }
    return myPizzas;
  }
}