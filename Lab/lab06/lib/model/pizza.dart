class Pizza {
  late int? id;
  late String name;
  late String? description;
  late double? price;
  late String? imageUrl;

  Pizza.createDefault(){
    id = null;
    name = '';
    description = '';
    price = null;
    imageUrl = '';
  }

  Pizza({
    required this.id,
    required this.name,
    required this.description,
    required this.price,
    required this.imageUrl,
  });

  Pizza.fromJson(Map<String, dynamic> json){
    this.id = json['id'] ?? 0;
    this.name = json['pizzaName'] ?? '';
    this.description = json['description'] ?? '';
    this.price = json['price'] ?? 0.0;
    this.imageUrl = json['imageUrl'] ?? '';
  }

  Map<String, dynamic> toJson(){
    return {
      'id': id,
      'pizzaName': name,
      'description': description,
      'price': price,
      'imageUrl': imageUrl
    };
  }

}