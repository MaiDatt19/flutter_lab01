import 'package:flutter/material.dart';
import 'package:lab03bai01/stream/text_stream.dart';


class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Text Stream",
        home: Scaffold(
          body: TextScreen(),
        )
    );
  }

}

class TextScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return TextState();

  }

}

class TextState extends State<StatefulWidget>{
  TextStream messageStream = TextStream();
  Text streamText = Text('Messages is streaming');


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Messages',

      home: Scaffold(
        appBar: AppBar(title: Text('Streaming'),),
        body: streamText,
        floatingActionButton: FloatingActionButton(
          child: Text('Start'),
          onPressed: (){
            streamMessages();
          }
          ),
        ),
    );
  }

  streamMessages() async{
    messageStream.getMessages().listen((event) {
      setState(() {
        streamText = Text(event);
      });
    });
  }
  
}