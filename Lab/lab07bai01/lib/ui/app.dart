import 'package:flutter/material.dart';
import 'package:lab07bai01/ui/home.dart';
import 'package:lab07bai01/ui/setting.dart';


class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/':(context)=> AppHomePage()
      },
      initialRoute: '/',
    );
  }
  
}

class AppHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }

}

class AppHomePageState extends State<AppHomePage>{
  int currentIndex = 0;
  final screens = [
    HomeScreen(),
    SettingScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Parking Lot'),
        ),
        body: IndexedStack(
          children: screens,
          index: currentIndex,
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home'
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings'
            )
          ],
          currentIndex: currentIndex,
          onTap: (index){
            setState(() {
              currentIndex = index;
            });
          }
        ),
      )
    );
  }
}