import 'package:flutter/material.dart';


class PlanCreatorScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return PlanCreatorScreenState();
  }

}

class PlanCreatorScreenState extends State<StatefulWidget>{
  final planController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My Plan')),
      body: Column(
        children: [
          buildListCreator()
        ],
      ),
    );
  }

  Widget buildListCreator(){
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Material(
        child: TextField(
          controller: planController,
          decoration: InputDecoration(labelText: 'Add a plan'),
        ),
      ),
    );
  }
}