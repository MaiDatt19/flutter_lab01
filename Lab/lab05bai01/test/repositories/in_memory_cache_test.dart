import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../lib/repositories/repositories.dart';
import '../../lib/repositories/in_memory_cache.dart';

void main(){
  Repository repo = InmemoryCache();
  test('TEst In Memory Repository', (){
    expect(repo.getAll().isEmpty, true);

    Model newModel = repo.create();

    expect(newModel.id, 1);
    expect(repo.getAll().isEmpty, false);

    Model? existedModel = repo.get(1);

    expect(newModel, existedModel);

    Model newModel2 = repo.create();
    expect(repo.getAll().length, 2);
  });

  test('Test Model', () {
    Model newModel = Model(id: 3, data: {'task_name':'Finish all assignments'});

    expect(newModel.id, 3);
    expect(newModel.data.isEmpty, false);
    expect(newModel.data['task_name'], 'Finish all assignments');
  });

}