import 'dart:async';

class Bloc {
  final _emailStreamController = StreamController();

  StreamController getEmailStreamController(){
    return _emailStreamController;
  }

  //get emailStreamController => _emailStreamController;
  //get changeEmail => _emailStreamController.sink.add;
  Function(String) get changeEmail => _emailStreamController.sink.add;
  Stream get emailStream => _emailStreamController.stream;


  Bloc(){
    _emailStreamController.stream.listen((event) {
      print('Listening...: $event');
      });
  }
}