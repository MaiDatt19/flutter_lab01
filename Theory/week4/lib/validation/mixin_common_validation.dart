mixin CommonValidation{
  String? validateEmail(String? value){
    if(value!.isEmpty){
      return "Email is required.";
    }
    else if(!value.contains('@')){
      return "Invalid email (Missing '@')";
    }
    else if (!value.contains('.')){
      return "Invalid email (Missing '.')";
    }
    else{
      return null;
    }
  }

  String? validatePassword(String? value){
    if(value!.isEmpty){
      return "Password is required.";
    }
    else if(!value.contains(RegExp(r'[A-Z]'))){
      return "Password needs at least 1 capital letter.";
    }
    else if(!value.contains(RegExp(r'[a-z]'))){
      return "Password needs at least 1 lowercase letter.";
    }
    else if(!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))){
      return "Password needs at least 1 special letter.";
    }
    else if(value.length < 8){
      return "Password must have at least 8 characters.";
    }
    else{
      return null;
    }
  }
}